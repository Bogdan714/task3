package com.example.lenovo.androidparta3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.Login)
    EditText login;

    @BindView(R.id.phone)
    EditText phone;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.password)
    EditText password1;

    @BindView(R.id.passwordCheck)
    EditText password2;

    //@BindView(R.id.done)
    //Button doneButton;

    @BindView(R.id.message)
    TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.done)
    public void onClickDone(){
        if(login.getText().toString().length() < 1){
            message.setText("invalid login");
            return;
        }
        if(!Checker.isEmailValid(email.getText().toString())){
            message.setText("invalid email");
            return;
        }
        if(!Checker.isPhoneValid(phone.getText().toString(),10)){
            message.setText("invalid phone number");
            return;
        }
        if(!(password1.getText().toString()).equals(password2.getText().toString())){
            message.setText("Passwords don't match");
            return;
        }
        message.setText("done!");
    }
}

package com.example.lenovo.androidparta3;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;

public class Checker {
    public static boolean isPhoneValid(@Nullable String phone, int length){
        return !TextUtils.isEmpty(phone) && !phone.contains(" ") && !phone.contains("+")
                && phone.length() >= length && Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isEmailValid(@Nullable String email){
    return email.contains("@") && email.contains(".");
    }
}
